package ge.lessions.first;

public class Heap {
	
    private int arr [] = new int[30];
    private int count = 0;
    private int parentIndex;
    
    public static void main(String[] args) {
         
          Heap heap = new Heap();
          
          heap.push(1);
          heap.push(2);
          
          heap.push(4);
          heap.push(5);
           
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          heap.push(3);
          heap.push(9);
          heap.push(6);
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          heap.push(12);
          heap.push(29);
          heap.push(15);
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          System.out.println(heap.getMinAndPoll());
          
          
          for (int i : heap.arr) {
        	  if (i!=0)
        		  System.out.print(i + " ");
          }
        
    }

    public void push(int val) {
        
    	this.ensureCapacity();
    	
    	int index = 0;
    	
    	if(parentIndex == 0) {
    		parentIndex++;
    		index = parentIndex;
    		arr[index] = val;
    		
    	} else {
    		
    		if (this.left()) {
    			
    			index = parentIndex*2;
    			arr[index] = val;
    			
    		} else {
    			
    			index = parentIndex*2+1;
    			arr[index] = val;
    			parentIndex++;
    		}
    	}
    
        this.moveUp(index, val);
        count ++;
    }
   
    
    
    private void moveUp(int index, int val) {
    	
    	int parentIndex = index / 2;
    	if (parentIndex == 0) {
    		parentIndex = 1;
    	}
    	
        if(this.arr[parentIndex] > val) {
        	int tmp = this.arr[parentIndex];
            this.arr[index] = tmp;
            this.arr[parentIndex] = val;
            this.moveUp(parentIndex, val);
        }
    }
   
    public int getMinAndPoll() {
    	
    	int min = arr.length > 0 ? arr[1] : 0;
    	
    	this.popUp(1);
    	count--;
        return min;
    }
    
    
    private void popUp(int parent) {
    	
    	if (count > 0) {
    		
    		int ind = count;
    		
    		if (ind > 2) {
    			
    			if (arr[parent*2+1]!=0) {
    				ind = arr[parent*2] < arr[parent*2+1] ? parent*2 : parent*2+1;
    			} else {
    				ind = parent*2;	
    			}
    			
    			if (ind < count) {
        			
        			arr[parent] = arr[ind];
        			this.popUp(ind);
        			this.parentIndex = parent;
            		
        		} else {
        			arr[parent] = arr[count];
        			arr[count] = 0;
        		} 
    		} else {
    			arr[parent] = arr[ind];
    			this.parentIndex = parent;
    			this.arr[count] = 0;
    		}
    		
    	}
    	
   }

    private boolean left() {
    	return (this.count == 0 || this.count == 1 || this.count % 2 != 0);
    }
    
    private void ensureCapacity() {
    	
    	int c = arr.length;
    	if(arr.length - count <= 2) {
    		c *= 2;
    		int tmp[] = new int[c];
            for (int i = 0; i < arr.length; i++) {
                  tmp[i] = arr[i];
            }
            
            arr = tmp;
    	}
        
    }
   
}